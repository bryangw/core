﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Website.Controllers
{
    public class BaseController : Controller
    {
        public Data.Context db { get;set;}
        public BaseController()
        {
            db = new Data.Context();
            HttpContext.Items["db"] = db;
        }
        public T Get<T>(Guid id) where T : Models.EntityBase, new()
        {
            if(id == Guid.Empty)
            {
                return new T();
            }
            return db.Set<T>().Find(id);
        }
        public T Save<T>(T Obj) where T : Models.EntityBase
        {
            if (Obj.Id == Guid.Empty)
            {
                db.Set<T>().Add(Obj);
                db.SaveChanges();
                return Obj;
            }
            if (db.Entry(Obj).State != System.Data.Entity.EntityState.Detached)
            {
                db.SaveChanges();
                return Obj;
            }
            var dbObj = db.Set<T>().Find(Obj.Id);
            db.Entry(dbObj).OriginalValues.SetValues(Obj);
            db.SaveChanges();
            return dbObj;
        }
        public ActionResult View(object model)
        {
            return base.View("~/Views/Shared/Layout.cshtml", model);
        }
        protected override void Dispose(bool disposing)
        {
            if (Core.Application.SessionCacheType == Core.Application.SessionCacheTypes.SQLCache && HttpContext.Items["SessionWasFetched"] != null)
            {
                var s = Core.Application.GetSession();
                if (Session["SessionId"] == null)
                {
                    var ss = new Models.Core.SessionStore()
                    {
                        JsonCache = s.toJSON()
                    };
                    db.SessionStores.Add(ss);
                    db.SaveChanges();
                    base.Dispose(disposing);
                    return;
                }
                var guid = (Guid)Session["SessionId"];
                var SessionStore = db.SessionStores.Where(ss => ss.Id == guid).First();
                SessionStore.JsonCache = s.toJSON();
                db.SaveChanges();
                base.Dispose(disposing);
                return;
            }
            base.Dispose(disposing);
        }
    }
}