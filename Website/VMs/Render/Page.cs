﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.VMs.Render
{
    public class Page
    {
        public PageMeta Meta { get; set; }
        public PageTypes PageType { get; set; }
        public Page(PageTypes PageType)
        {
            Meta = new PageMeta();
            this.PageType = PageType;
        }
    }
    public enum PageTypes {
        Public = 1, 
        User = 2,
        Establishment = 3, 
        Administration = 4 
    }
    public class PageMeta
    {
        public PageMeta()
        {
            Title = "Application and Website Name";
            Keywords = "Bar, Restuarant, Bar Specials, Restaurant Specials, Happy Hour, Drinks";
            Description = "Site Name:  Find Bar and Restaurants specials whereever and whenever you are.";
        }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
    }
}