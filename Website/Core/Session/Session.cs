﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.Authorization;
namespace Website.Core
{
    public class Session
    {
        public User User {get;set;}
        public List<Permission> Permissions { get; set; }
    }
}