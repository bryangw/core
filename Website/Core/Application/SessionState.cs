﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Website.Core
{
    public static partial class Application
    {
        public const SessionCacheTypes SessionCacheType = SessionCacheTypes.MemCache;
        public static Core.Session GetSession()
        {
            var cc = HttpContext.Current;
            if (SessionCacheType == SessionCacheTypes.MemCache)
            {
                if (cc.Session["Session"] == null)
                {
                    cc.Session["Session"] = new Session();
                }
                return (Session)cc.Session["Session"];
            }
            cc.Items["SessionWasFetched"] = true;
            if (cc.Items["Session"] != null)
            {
                return (Session)cc.Items["Session"];
            }
            Data.Context db;
            Session s;
            if (cc.Session["SessionId"] == null)
            {
                s = new Session();
                cc.Items["Session"] = s;
                return s;
            }
            if (HttpContext.Current.Items["db"] != null)
            {
                db = (Data.Context)HttpContext.Current.Items["db"];
            }
            else
            {
                db = new Data.Context();
            }

            var guid = (Guid)cc.Session["SessionId"];
            s = db.SessionStores.Where(sState => sState.Id == guid).First().JsonCache.Deserialize<Session>();
            cc.Items["Session"] = s;
            return s;
        }
        public enum SessionCacheTypes
        {
            MemCache = 1,
            SQLCache = 2
        }
    }
}