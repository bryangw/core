﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace Website
{
    public static class ObjectExtensions
    {
        private static JsonSerializerSettings _SerializerSettings { get; set; }
        private static JsonSerializerSettings SerializerSettings
        {
            get
            {
                if (_SerializerSettings == null)
                {
                    lock (_SerializerSettings)
                    {
                        _SerializerSettings = new JsonSerializerSettings();
                        SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    }
                }
                return SerializerSettings;
            }
        }
        private static string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, SerializerSettings);
        }
        public static string toJSON(this Object target)
        {
            return Serialize(target);
        }
        public static string toJSONAjax(this Object target)
        {
            return Serialize(new { Result = true, data = target });
        }
        
    }
}