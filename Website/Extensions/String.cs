﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website
{
    public static class StringExtensions
    {
        public static T Deserialize<T>(this string target)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(target);
        }
    }
}