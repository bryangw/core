﻿using System.Web;
using System.Web.Optimization;

namespace Website
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Bundles/Public"));
            bundles.Add(
                new StyleBundle("~/Bundles/Admin")
                        .Include("~/FlatDreambootstrap.css")
                        .Include("~/FlatDreamStyle.css")
                );
            BundleTable.EnableOptimizations = true;
        }
    }
}
