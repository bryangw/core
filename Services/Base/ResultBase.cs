﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Services
{
    public class ResultBase
    {
        public bool Result { get; set; }
        public List<ResultError> Errors { get; set; }
        public ResultBase()
        {
            Errors = new List<ResultError>();
        }
        public void AddError(string Type, string Message, bool isSystemError = false, [CallerMemberName] string Method = "", [CallerFilePath] string Source = "", [CallerLineNumber] int LineNumber = 0)
        {
            var function = new StackFrame(1, true).GetMethod().Name;
            Errors.Add(new ResultError()
            {
                Type = Type,
                Message = Message,
                isSystemError = isSystemError,
                Method = Method,
                Source = Source,
                LineNumber = LineNumber
            });
        }
    }
    public class ResultError
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
        public string Method { get; set; }
        public int LineNumber { get; set; }
        public bool isSystemError { get; set; }
    }
}

