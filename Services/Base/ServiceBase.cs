﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    private class ServiceBase
    {
        protected Data.Context db { get; set; }
        protected ServiceBase(Data.Context db)
        {
            this.db = db;
        }
        protected void CheckDB(){
            if (db == null)
            {
                throw new Exception("This service call requires a data context");
            }
        }
    }
}
