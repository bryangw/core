﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Accounts.Results
{
    public class Authenticate : ResultBase
    {
        public Models.Authorization.User User { get; set; }
    }
}
