﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Accounts
{
    public class Authorization : ServiceBase
    {
        public Authorization(Data.Context db = null)
            : base(db)
        {

        }
        public string EncryptPassword(string Password)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(Password);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            String hash = System.Text.Encoding.ASCII.GetString(data);
            return hash;
        }
        public bool CheckAccountExists(string email)
        {
            CheckDB();
            email = email.ToLower();
            return db.Users.Any(u => u.Email == email);
        }
        public Results.Authenticate Authenticate(string email, string Password)
        {
            Results.Authenticate Result = null;
            CheckDB();
            email = email.ToLower();
            Password = EncryptPassword(Password);
            var User = db.Users.Where(u => u.Email == email && u.Password == Password).FirstOrDefault();
            if (User != null)
            {
                Result = new Results.Authenticate() { User = User, Result = true };
                return Result;
            }
            if (!CheckAccountExists(email))
            {
                Result = new Results.Authenticate();
                Result.AddError("Email", "The email address you entered wasn't found in our database.");
                return Result;
            }
            Result = new Results.Authenticate();
            Result.AddError("Password", "The password you entered is incorrect");
            return Result;
        }

    }
}
