﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Models;
using System.Data.Entity.ModelConfiguration;
namespace Data.Configurations.Core
{
    public class List : EntityTypeConfiguration<Models.Core.List>
    {
        public List()
        {
            HasMany(l => l.Items).WithRequired();
        }
    }
}
