﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Data.Configurations.Authorization;
namespace Data.Configurations.Core
{
    public static class Configuration
    {
        public static void Configure(DbModelBuilder ModelBuilder)
        {
            ModelBuilder.Configurations.Add(new List());
            ModelBuilder.Configurations.Add(new ListItem());
            ModelBuilder.Configurations.Add(new SessionStore());
        }
    }
}
