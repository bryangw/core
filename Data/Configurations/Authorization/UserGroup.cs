﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
namespace Data.Configurations.Authorization
{
    class UserGroup : EntityTypeConfiguration<Models.Authorization.UserGroup>
    {
        public UserGroup()
        {
            HasMany(g => g.PermissionLinks).WithOptional();
        }
    }
}
