﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Authorization;
using System.Data.Entity.ModelConfiguration;
namespace Data.Configurations.Authorization
{
    public class PermissionLink : EntityTypeConfiguration<Models.Authorization.PermissionLink>
    {
        public PermissionLink()
        {
            ToTable("PermissionLinks");
        }
    }
}

