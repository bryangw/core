namespace Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Configurations.Core.Configuration.Configure(modelBuilder);
            Configurations.Authorization.Configuration.Configure(modelBuilder);
        }
    }
}
