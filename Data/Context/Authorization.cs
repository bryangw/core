﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Authorization = Models.Authorization;
namespace Data
{
    public partial class Context
    {
        public DbSet<Authorization.User> Users { get; set; }
        public DbSet<Authorization.UserGroup> UserGroups { get; set; }
        public DbSet<Authorization.Permission> Permissions { get; set; }
        public DbSet<Authorization.PermissionLink> PermissionLink { get; set; }
    }
}
