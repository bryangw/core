﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Core = Models.Core;
namespace Data
{
    public partial class Context
    {
        public DbSet<Core.List> Lists { get; set; }
        public DbSet<Core.ListItem> ListItems { get; set; }
        public DbSet<Core.SessionStore> SessionStores { get; set; }
    }
}
