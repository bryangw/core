﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Types
{
    public class eList<T> : List<T> 
    {
        public void AddDistinct(T Obj)
        {
            if (!Contains(Obj))
            {
                Add(Obj);
            }
        }
        public void AddRangeDistinct(List<T> ObjList)
        {
            ObjList.ForEach(i => AddDistinct(i));
        }
        public void SafeRemove(T Obj)
        {
            if (Contains(Obj))
            {
                Remove(Obj);
            }
        }
        public void SafeRemoveRange(List<T> ObjList)
        {
            ObjList.ForEach(o => SafeRemove(o));
        }
    }
}
