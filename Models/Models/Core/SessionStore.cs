﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace Models.Core
{
    public class SessionStore : EntityBase
    {
        [MaxLength]
        [Column(TypeName="varchar(max)")]
        public string JsonCache { get; set; }
    }
}
