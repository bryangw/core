﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Core
{
    public class List : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<ListItem> Items { get; set; }
    }
}
