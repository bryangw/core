﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Core
{
    public class ListItem : EntityBase
    {
        public Guid ListId { get; set; }            
        public string Label { get; set; }
        public string Description { get; set; }
    }
}
