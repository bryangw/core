﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Interfaces;
using Models.Helpers.Authorization;
using System.ComponentModel.DataAnnotations.Schema;
using Models.Types;
namespace Models.Authorization
{
    public class UserGroup : EntityBase, IhasPermissions
    {
        public virtual List<User> Users { get; set; }
        public virtual List<PermissionLink> PermissionLinks { get; set; }
        public string Name { get; set; }
        [NotMapped]
        private List<Permission> _Permissions { get; set; }
        [NotMapped]
        public List<Permission> Permissions
        {
            get
            {
                if (_Permissions == null)
                {
                    _Permissions = this.GetPermissions() as List<Permission>;
                }
                return _Permissions;
            }
        }

    }
}

