﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Types;
using Models.Helpers.Authorization;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Authorization
{
    public class User : EntityBase, Interfaces.IhasPermissions
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public virtual List<UserGroup> Groups { get; set; }
        public virtual List<PermissionLink> PermissionLinks { get; set; }
        [NotMapped]
        private List<Permission> _Permissions { get; set; }
        [NotMapped]
        public List<Permission> Permissions
        {
            get
            {
                if(_Permissions==null){
                    _Permissions = GetUserPermissions();
                }
                return _Permissions;
            }
        }
        private List<Permission> GetUserPermissions()
        {
            var eList = new eList<Permission>();
            Groups.ForEach(g=>eList.AddRangeDistinct(g.Permissions));
            return this.GetPermissions(eList) as List<Permission>;
        }
        private bool CheckPermission(string PermissionName)
        {
            return Permissions.Any(p => p.Name == PermissionName);
        }
            

    }
}
