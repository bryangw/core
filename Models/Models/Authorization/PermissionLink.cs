﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Authorization
{
    public class PermissionLink : EntityBase
    {
        public Guid UserId { get; set; }
        public Guid UserGroupId { get; set; }
        public Guid PermissionId { get; set; }
        public Permission Permission { get; set; }
        public bool isExclusion { get; set; }
    }
}
