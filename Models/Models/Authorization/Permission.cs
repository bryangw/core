﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Authorization
{
    public class Permission :EntityBase
    {
        public string Name { get; set; }
    }
}
