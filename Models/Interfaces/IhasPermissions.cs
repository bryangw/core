﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Authorization;
using Models.Types;
namespace Models.Interfaces
{
    public interface IhasPermissions
    {
        List<PermissionLink> PermissionLinks { get; set; }
    }
}
