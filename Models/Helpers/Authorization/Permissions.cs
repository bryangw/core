﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Types;
using Models.Authorization;
namespace Models.Helpers.Authorization
{
    public static class ExtensionMethods
    {
        public static eList<Permission> GetPermissions(this Interfaces.IhasPermissions target, eList<Permission> Permissions = null)
        {
            if (Permissions == null)
            {
                Permissions = new eList<Permission>();
            }
            target.PermissionLinks.ToList();
            Permissions.AddRangeDistinct(
                target.PermissionLinks
                      .Where(pl => pl.isExclusion == false)
                      .Select(pl => pl.Permission)
                      .ToList()
            );
            Permissions.SafeRemoveRange(
                target.PermissionLinks
                      .Where(pl => pl.isExclusion == true)
                      .Select(pl => pl.Permission)
                      .ToList()
            );
            return Permissions;
        }
    }
}
